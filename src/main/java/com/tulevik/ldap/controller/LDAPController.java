package com.tulevik.ldap.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tulevik.ldap.service.LDAPService;

 
@Controller
@RequestMapping("/")
public class LDAPController {
	
	@Autowired
	LDAPService lDAPService;
	
	
	@GetMapping("/")
	public String index(HttpSession session,HttpServletRequest request,Model model) {
		return "/ldap";
	}
	
	@PostMapping("/checkLDAP")
	public String ldapTest(HttpSession session,HttpServletRequest request,Model model) {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String ldapurl = request.getParameter("ldapurl");
		String basedn = request.getParameter("basedn");
		String dnpattern = request.getParameter("pattern");
		String obj = request.getParameter("obj");
		String opt = request.getParameter("opt");
		
		String res = "";
	 	if(opt.equals("opt1")) {
	 		res = lDAPService.checkLDAP(username,password,ldapurl, basedn, dnpattern, obj);
	 	}else {
	 		res = lDAPService.checkLDAP2(username,password,ldapurl, basedn, dnpattern, obj);
	 	}
		
		
		 model.addAttribute("username", username);
		 model.addAttribute("ldapurl", ldapurl);
		 model.addAttribute("basedn", basedn);
		 model.addAttribute("dnpattern", dnpattern);
		 model.addAttribute("obj", obj);
		 model.addAttribute("opt", opt);
		 model.addAttribute("res", res);
	      
	        
		return "/ldap";
	}
}
