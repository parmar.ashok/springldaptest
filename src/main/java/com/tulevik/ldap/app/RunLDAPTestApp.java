package com.tulevik.ldap.app;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.WebApplicationInitializer;

 

@SpringBootApplication
@ComponentScan("com.tulevik.ldap")
 
public class RunLDAPTestApp extends SpringBootServletInitializer /* implements WebApplicationInitializer */{
public static final Logger logger = LoggerFactory.getLogger(RunLDAPTestApp.class);
	
	public static void main(String[] args) {
		logger.info("########################################################");
		logger.info(":: PDF Bill Reader APIs        ::        (v.1.0.RELEASE)");
		logger.info(":: Build Date            		::        (07-11-2018 12:30)");
		logger.info(":: JAVA                  		::        ("+System.getProperty("java.version")+")");
		
		logger.info("########################################################");
		logger.info(":: TULEVIK SOLUTIONS LLP 	::  ");
		logger.info(":: Developer  				::        (ASHOK PARMAR)");
		logger.info(":: EMAIL      				::        (tuleviksolutions@gmail.com)");
		logger.info("########################################################");
		new SpringApplicationBuilder(RunLDAPTestApp.class).sources(RunLDAPTestApp.class)
        .properties(getProperties())
      //  .web(WebApplicationType.NONE)
        .run(args);
	}

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RunLDAPTestApp.class)
        		.properties(getProperties());
    }
	
	static Properties getProperties() {
	      Properties props = new Properties();
	      props.put("spring.config.location", "classpath:tulevikweb/");
	      return props;
	   }
}
