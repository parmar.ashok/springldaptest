package com.tulevik.ldap.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.ldap.core.CollectingAuthenticationErrorCallback;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

@Service
public class LDAPService {

	
	public String checkLDAP2(String username,String password,String url,String basedn,String pattern,String objectclass) {
		StringBuilder sb = new StringBuilder();
		
		String strDn = pattern.replace("{username}", username);
		
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		
		//env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL,strDn);
		env.put(Context.SECURITY_CREDENTIALS, password);
		 
		 NamingEnumeration<?> results = null;
		 DirContext ctx = null;
		try {
		    ctx = new InitialDirContext(env);
		    sb.append(" <br/>*** connected");
		    sb.append(" <br/>***  "+ctx.getEnvironment());
		    
		    sb.append(" <br/>***  searching...");
		    
		    
		    SearchControls controls = new SearchControls();
		    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		    
		    
		   // String filter = "uid=galieleo,ou=scientists,dc=example,dc=com"; // "(&(objectclass=mathematicians)(uid=" + userId + ")(userPassword=" + userPwd + "))";
		   // String filter = "(&(objectclass=User)(uid=" + userId + ")(userPassword=" + userpassword + "))";
		  
		 // String filter = "(&(objectclass=person)&(uid=" + username + "))";
		 //   String filter = "(&(objectclass="+objectclass+")&(uid=" + username + "))";
		    String filter = "";
		    if(objectclass != null && !objectclass.isEmpty()) {
		    	filter = "(&(objectclass="+objectclass+")&(uid=" + username + "))";
		    }else {
		    	filter = "(&(uid=" + username + "))";
		    }
		    sb.append(" <br/>*** filter..."+filter);
		//    String filter = "(&(objectclass=person)&(uid=" + username + ")(userPass="+ userpassword +") )";
		    
		    results = ctx.search(strDn, filter, controls);
		     
		      if (results.hasMore()) {
		    	  sb.append(" ***<br/> User found");
		      } else {
		    	  sb.append(" ***<br/> User not found");
		      }
		      if(results.hasMore()) {
		          Attributes attrs = ((SearchResult) results.next()).getAttributes();
		          sb.append(" <br/>*** mail: " + attrs.get("mail").get());
		          sb.append(" <br/>*** cn: " + attrs.get("cn").get());
		          //System.out.println("cn: " + attrs.get("userPass").get());
		        }
		    
		    // do something useful with the context...
		   
		 
		} catch (AuthenticationNotSupportedException ex) {
			sb.append(" <br/>***  The authentication is not supported by the server");
		} catch (AuthenticationException ex) {
			sb.append(" <br/>***  incorrect password or username");
		} catch (NamingException ex) {
			ex.printStackTrace();
			sb.append(" ***<br/>  error when trying to create the context");
		}finally{
			if (results != null) {
		        try {
		          results.close();
		        } catch (Exception e) {
		        }
		      }

		      if (ctx != null) {
		        try {
		        	ctx.close();
		        } catch (Exception e) {
		        }
		      }
		}
		
		String v = sb.toString();
		sb = null;
		return v;
	}
		
	
	public String checkLDAP(String username,String password,String url,String basedn,String pattern,String objectclass) {
		StringBuilder sb = new StringBuilder();
		String strDn = pattern.replace("{username}", username);
		try {
			LdapTemplate ldapTemplate = ldapTemplate( url, strDn, username);
			
			//if(domain.equalsIgnoreCase("ibank")){
				if(ldapTemplate == null) {
					sb.append(" ***<br/> LdapTemplate is NULL.... ");
				}
				AndFilter filter = new AndFilter();
				 if(!objectclass.isEmpty()) {
					 filter.and(new EqualsFilter("objectclass",objectclass ));
				 }
				filter.and(new EqualsFilter("uid",username ));
				
				//filter.and(new EqualsFilter("uid", userName));
				//filter.and(new EqualsFilter("userPassword", password));
			//	String strDn = pattern.replace("{username}", username);
				
				sb.append("<br/> *** AndFilter "+filter.encode()); 
			//	LOGGER.info(" *** LdapTemplate "+ldapTemplate.toString());
			  
				sb.append("<br/> *** LdapTemplate ldap.user.dn.pattern .... "+strDn);
			//    LOGGER.info(" *** LdapTemplate password .... "+password);
			     
				//isValid =ldapTemplate.authenticate(ldapBaseDn,strDn, password);
				//isValid = ldapTemplate.authenticate(strDn, filter.toString(), password);
			    CollectingAuthenticationErrorCallback errorCallback = new CollectingAuthenticationErrorCallback();
			    sb.append("<br/> *** errorCallback .... "+errorCallback.toString());
			   boolean isValid = ldapTemplate.authenticate(LdapUtils.emptyLdapName(), filter.toString(), password, errorCallback);
			  // boolean isValid =  ldapTemplate.authenticate(LdapUtils.emptyLdapName(), filter.toString(), password);
		 
			    if(isValid)
			    	sb.append("<br/> *** SUCCESS ");
			    else
			    	sb.append("<br/> *** FAIL ");
 
			//} 
		}catch(Exception e) {
			//e.printStackTrace();
			sb.append("<br/>  *** LdapTemplate Exception .... "+e.getMessage());
			 
					StringWriter sw = new StringWriter();
			        e.printStackTrace(new PrintWriter(sw));
			        String exceptionAsString = sw.toString();
			        sb.append("<br/> ERROR : "+exceptionAsString);	 
				
		}
		
		String v = sb.toString();
		sb = null;
		return v;
	}
	    public LdapContextSource contextSource (String url,String base,String user) {
	        LdapContextSource contextSource= new LdapContextSource();
	        contextSource.setContextFactory(com.sun.jndi.ldap.LdapCtxFactory.class);
	        contextSource.setUrl(url);
	        contextSource.setBase(base);
	        contextSource.setUserDn(user);
	        contextSource.setAnonymousReadOnly(true);
	       // contextSource.setPassword("password");
	        return contextSource;
	    }
	    public LdapTemplate ldapTemplate(String url,String base,String user) throws Exception {
	        //return new LdapTemplate(contextSource(url, base, user)); 
	    	LdapContextSource contextSource = contextSource(url, base, user);
	    	LdapTemplate ldapSpringTemplate = new LdapTemplate();
	    	contextSource.afterPropertiesSet();
	    	ldapSpringTemplate.setContextSource( contextSource );
	    	ldapSpringTemplate.afterPropertiesSet();
	    	return ldapSpringTemplate;
	    }
}
