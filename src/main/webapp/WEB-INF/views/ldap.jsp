<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>LDAP Testing</h2>



<form action="checkLDAP" method="post">
  <label for="fname">LDAP URL:</label> 
  <input type="text" id="ldapurl" name="ldapurl" value="ldap://ldap.forumsys.com:389/"><br><br>
  <label for="lname">LDAP Base DN :</label> 
  <input type="text" id="basedn" name="basedn" value="cn=read-only-admin,dc=example,dc=com"><br><br>
  <label for="lname">LDAP DN Pattern:</label> 
  <input type="text" id="pattern" name="pattern" value="uid={username},dc=example,dc=com"><br><br>
  <label for="lname">LDAP Object Class :</label> 
  <input type="text" id="obj" name="obj" value="person"><br><br>
  <hr>
  <label for="lname">Username :</label> 
  <input type="text" id="username" name="username" value="euler"><br><br>
  <label for="lname">Password :</label> 
  <input type="password" id="password" name="password" value="password"><br><br>
  
	<input type="radio" id="opt1" name="opt" value="opt1" >
	<label for="male">LDAP Option 1</label><br> 
	<input type="radio" id=opt2 name="opt" value="opt2" checked>
	<label for="female">LDAP Option 2</label><br>

  <input type="submit" value="Submit">
  
</form>
  <hr>
 <h2>LDAP Response</h2>

	<ul>
	<li>LDAP URL : ${ldapurl}</li>
	<li>LDAP Base DN : ${basedn}</li>
	<li>LDAP DN Pattern : ${dnpattern}</li>
	<li>LDAP Object Class : ${obj}</li>
	<li>Username : ${username}</li>
	<li>LDAP Option : ${opt}</li>
	 
	</ul>
	<p>Response: ${res}
	</p>

</body>
</html>